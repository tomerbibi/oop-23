﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___23
{
    class Customer
    {
        public Customer(int id, string name, int birthYear, string adress, int protection)
        {
            Id = id;
            Name = name;
            BirthYear = birthYear;
            Adress = adress;
            Protection = protection;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int BirthYear { get; set; }
        public string Adress { get; set; }
        public int Protection { get; set; }
        public int TotalPurchases { get; set; }

        public override string ToString()
        {
            return $"ID: {Id}, name: {Name}, birth year: {BirthYear}, adress: {Adress}, protection: {Protection}, total purchases: {TotalPurchases}";
        }
    }
}
