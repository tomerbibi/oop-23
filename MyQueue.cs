using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___23
{
    class MyQueue
    {
        private List<Customer> _list = new List<Customer>();

        public MyQueue(List<Customer> list)
        {
            _list = list;
        }
        public int count
        {
            get
            {
                return _list.Count;
            }
        }

        public void Enqeueue (Customer c)
        {
            _list.Add(c);
        }
        public Customer Dequeue ()
        {
            Customer c = null;
            if (_list[0] != null)
            {
                c = _list[0];
                _list.RemoveAt(0);
            }
            return c;
        }
        public void Init(List<Customer> l)
        {
            _list = l;
        }
        public void clear()
        {
            _list.Clear();
        }
        public Customer WhoNext()
        {
            return _list[0];
        }
        public void SortByProtection()
        {
            CustomerCompareByProtection c = new CustomerCompareByProtection();
            _list.Sort(c);

        }
        public void SortByTotalPurchases()
        {
            CustomerCompareByTotalPurchases c = new CustomerCompareByTotalPurchases();
            _list.Sort(c);
        }
        public void SortByBirthYear()
        {
            CustomerCompareByBirthYear c = new CustomerCompareByBirthYear();
            _list.Sort(c);
        }
        public List<Customer> DequeueCustomers(int x)
        {
            List<Customer> c = new List<Customer>();
            if (x <= _list.Count)
            {
                for (int i = 0; i < x; i++)
                {
                    c.Add(_list[i]);
                }
                _list.RemoveRange(0, x);
            }
            return c;
        }
        public void AniRakSheela(Customer c)
        {
            _list.Insert(0, c);
        }
        /* that one seems better than the one below it but by doing that im changing the list so i left it as a comment
               public Customer DequeueProtectzia()
        {
            SortByTotalPurchases();
            return _list[0];
        }*/

        public Customer DequeueProtectzia()
        {
            int max = 0;
            Customer c = null;
            int index = 0;
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].Protection > max)
                {
                    max = _list[i].Protection;
                    c = _list[i];
                    index = i;
                }
            }
            _list.Remove(_list[index]);
            return c;
        }

    }
}
