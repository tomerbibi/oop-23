﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___23
{
    class CustomerCompareByBirthYear : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return (-1) * x.BirthYear.CompareTo(y.BirthYear);
        }
    }
}
