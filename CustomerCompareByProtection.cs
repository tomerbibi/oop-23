﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oop___23
{
    class CustomerCompareByProtection : IComparer<Customer>
    {
        public int Compare(Customer x, Customer y)
        {
            return x.Protection.CompareTo(y.Protection);
        }
    }
}
